variable vra_url {
  type = string
}

variable vra_refresh_token {
  type = string
}

variable "project_id" {
}

variable "blueprint_id" {
}

variable "blueprint_version" {
}

variable "deployment_name" {
}

variable "deployment_description" {
}

variable "image" {
}

variable "size" {
}

variable "username" {
}

variable "password" {
}

variable "workloadtype" {
}


