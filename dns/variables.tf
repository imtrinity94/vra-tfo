variable "record_name" {
  description = "hostname to create the DNS entry"
  default = "testa"
}

variable "record_addr" {
  description = "IP Address for the DNS entry "
  default = ""
}

variable "server" {
  description = "DNS Server "
  default = "10.79.140.10"
}

  
